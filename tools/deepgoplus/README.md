Welcome to the Wei2GO gitlab page! Wei2GO is a weighted sequence similarity and python-based open-source function prediction software. The pre-print can be found here: https://www.biorxiv.org/content/10.1101/2020.04.24.059501v1

[[_TOC_]]

### Running Wei2GO python script
To run Wei2GO, all you have to do is clone this gitlab project and download and gunzip the following file into the data folder:

    https://drive.google.com/drive/folders/1KFkwEYCQJ4PkyfVWhKH8Bd5KOduCbn2y

Install NumPy (https://scipy.org/install.html): 'python -m pip install --user numpy scipy matplotlib'.

Now just run 'python wei2go.py <diamond_input.tab> <hmmscan_input.tab> <outfile.tab>'.

DIAMOND input needs to be in default format, or --outfmt6 in case you want to run BLASTP instead. DIAMOND can be found here: http://www.diamondsearch.org/index.php

HMMER input is generated via hmmscan --tblout <hmmscan_output> Pfam-A.hmm <hmmscan_input>. HMMER can be found here: http://hmmer.org/download.html

**Please note that the '.all_preds' file is only given for provenance! This file includes predictions below the probability threshold of 500!**

### Wei2GO input generation and predictions using snakemake
There is also the option to run a snakemake file (J Köster, S Rahmann Bioinformatics 2012, https://snakemake.readthedocs.io/en/stable/). This requires you to install snakemake using 'sudo apt install snakemake'. When you run the snakemake file, it will generate the DIAMOND and HMMER input files. The output file will be outfiles/wei2go.tab. Make sure to delete this file if it exists before running this snakemake pipeline. There is some additional preparation needed to make sure these run smoothly:

1) Install DIAMOND http://www.diamondsearch.org/index.php

    `$ wget http://github.com/bbuchfink/diamond/releases/download/v0.9.31/diamond-linux64.tar.gz`
    
    `$ tar xzf diamond-linux64.tar.gz`

    `The extracted diamond binary file should be moved to a directory in your executable search path (PATH environment variable) (e.g. /usr/bin/ for Ubuntu users).`

2) Install the latest HMMER release from http://hmmer.org/download.html

3) Download the required databases for DIAMOND and HMMER

    `$ cd external_databases`

    `$ wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz`

    `$ wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz ->OPTIONAL BUT RECOMMENDED<-`

    `$ wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam33.0/Pfam-A.hmm.gz`

    `$ gunzip uniprot_sprot.fasta.gz`

    `$ gunzip uniprot_trembl.fasta.gz ->OPTIONAL BUT RECOMMENDED<-`

    `$ gunzip Pfam-A.hmm.gz`

4) If you decided to include the TrEMBL database for your DIAMOND search, first do

    `$ cat uniprot_sprot.fasta >> uniprot_trembl.fasta`

    `$ mv uniprot_trembl.fasta knowledgebase.fasta`

This appends the swissprot data to the trembl data file, and then renames it to knowledgebase.fasta

If you don't want to include TrEMBL, simply rename the uniprot_sprot.fasta file to knowledgebase.fasta

5) Create the DIAMOND and HMMER database files

    `$ diamond makedb --in knowledgebase.fasta --db knowledgebase`

    `$ hmmpress Pfam-A.hmm`

6) Now the only thing left to do, is to make sure the 'config.yaml' file has the correct paths set

    `fasta_filepath: 'path to your protein fasta file'`

    `diamond_uniprotdb: 'path to knowledgebase.fasta'`

    `hmmer_pfamdb: 'path to Pfam-A.hmm'`

    `goa_filepath: 'path to goa_uniprot_all.gaf'` <-- this one is not needed for running predictions with Wei2GO


### Updating the Wei2GO reference data using snakemake

1) To get the latest versions of data used by Wei2GO perform the following steps:

    `$ cd data/`

    `$ rm *`

    `$ wget http://current.geneontology.org/ontology/external2go/pfam2go`

    `$ wget http://current.geneontology.org/ontology/go.obo`

    `$ wget ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/goa_uniprot_all.gaf.gz`

    `$ gunzip goa_uniprot_all.gaf.gz`

    `$ cd ../`

    `$ snakemake -s update_databases.snakefile`


2) Make sure the 'config.yaml' file is set correctly for the following variable:

    `$ goa_filepath: data/goa_uniprot_all.gaf`