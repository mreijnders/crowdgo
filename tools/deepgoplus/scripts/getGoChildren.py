#!/usr/bin/python3
import sys
from collections import defaultdict

relationDict = defaultdict(list)
bool = False
for line in open(sys.argv[1]):
	if line.startswith('[Term]'):
		bool = True
		obsoleteBool = False
	elif bool == True and line.startswith('id:'):
		goTerm = line.strip().split(' ')[1]
	elif bool == True and line.startswith('is_a:'):
		goTermParent = line.strip().split(' ')[1]
		if obsoleteBool == False and goTerm.startswith('GO:'):
			relationDict[goTerm].append(goTermParent)
	elif bool == True and line.startswith('relationship: has_part'):
		goTermParent = line.strip().split(' ')[2]
		if obsoleteBool == False and goTerm.startswith('GO:'):
			relationDict[goTerm].append(goTermParent)
	elif bool == True and line.startswith('is_obsolete: true'):
		obsoleteBool = True

uppropagatedRelationDict = defaultdict(list)
for goTerm in list(relationDict):
	goTermParents = relationDict[goTerm]
	bool = True
	checkList = []
	iterationList = goTermParents
	while bool == True:
		for iterationTerm in iterationList:
			if not iterationTerm in checkList:
				checkList.append(iterationTerm)
				iterationTermParents = relationDict[iterationTerm]
				if iterationTermParents == []:
					bool = False
				else:
					for iterationTermParent in iterationTermParents:
						iterationList.append(iterationTermParent)
	uppropagatedRelationDict[goTerm] = iterationList

downpropagatedRelationDict = defaultdict(list)
for key,values in uppropagatedRelationDict.items():
	for value in set(values):
		downpropagatedRelationDict[value].append(key)

for key,values in downpropagatedRelationDict.items():
	for value in values:
		print(key+'\t'+value)
