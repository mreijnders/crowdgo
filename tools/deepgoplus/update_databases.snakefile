configfile: 'config.yaml'

rule all:
	input: 	#'data/goaUniprot_IEA.tab',
		#'data/goaUniprot_noIEA.tab',
		#'data/goCounts.tab',
		'data/nameSpaces.tab',
		'data/goParents.tab',
		'data/goChildren.tab'

#rule extract_uniprot:
#	input: config['goa_filepath']
#	output: 'data/goaUniprot_noIEA.tab',
#		'data/goaUniprot_IEA.tab'
#	shell: 'scripts/extractUniprot.py {input} data/'

#rule calculate_gocounts:
#	output: 'data/goCounts.tab'
#	shell: 'scripts/getGoCounts.py data/goaUniprot_IEA.tab data/goaUniprot_noIEA.tab data/go.obo > {output}'

rule extract_namespaces:
	output: 'data/nameSpaces.tab'
	shell: 'scripts/extractNamespaces.py data/go.obo > {output}'

rule get_goparents:
	output: 'data/goParents.tab'
	shell: 'scripts/getGoParents.py data/go.obo > data/goParents.tab'

rule get_gochildren:
	output: 'data/goChildren.tab'
	shell: 'scripts/getGoChildren.py data/go.obo > data/goChildren.tab'
