# Run diamond to get similar sequences
diamond blastp -d $3/data/train_data.dmnd -q $1 --outfmt 6 qseqid sseqid bitscore > $3/data/diamond.res

# Run prediction
python3 $3/predict.py -if $1 -of $2 -df $3/data/diamond.res
