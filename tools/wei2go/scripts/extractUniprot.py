#!/usr/bin/python3
import sys

evidenceList = ['EXP','IDA','IPI','IMP','IGI','IEP','TAS']
outdir = sys.argv[2]
ieaFile = open(outdir+'/goaUniprot_IEA.tab','w')
ieaFile.write('Protein\tGO term')
noIEAFile = open(outdir+'/goaUniprot_noIEA.tab','w')
noIEAFile.write('Protein\tGO term')
for line in open(sys.argv[1]):
	if line.startswith('UniProtKB'):
		ssline = line.split('\t')
		evidence = ssline[6]
		if evidence in evidenceList:
			if not ssline[1].strip() == '':
				noIEAFile.write('\n'+ssline[1].split('|')[0]+'\t'+ssline[4])
		else:
			if not ssline[1].strip() == '':
				ieaFile.write('\n'+ssline[1].split('|')[0]+'\t'+ssline[4])
ieaFile.close()
noIEAFile.close()
