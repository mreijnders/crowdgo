import sys

print('Counts\tType\tMethod')
for line in open(sys.argv[1]):
	tp,fp,bla = line.strip().split(' ')
	print(tp+'\tTrue Positives\tDeepGOPlus')
	print(fp+'\tFalse Positives\tDeepGOPlus')

for line in open(sys.argv[2]):
        tp,fp,bla = line.strip().split(' ')
        print(tp+'\tTrue Positives\tFunFams')
        print(fp+'\tFalse Positives\tFunFams')
for line in open(sys.argv[3]):
        tp,fp,bla = line.strip().split(' ')
        print(tp+'\tTrue Positives\tInterProScan')
        print(fp+'\tFalse Positives\tInterProScan')
for line in open(sys.argv[4]):
        tp,fp,bla = line.strip().split(' ')
        print(tp+'\tTrue Positives\tWei2GO')
        print(fp+'\tFalse Positives\tWei2GO')
for line in open(sys.argv[5]):
        tp,fp,bla = line.strip().split(' ')
        print(tp+'\tTrue Positives\tCrowdGO')
        print(fp+'\tFalse Positives\tCrowdGO')
