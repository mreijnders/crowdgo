import sys
import plotly.graph_objects as go
import plotly

label = ['True Positives: 4,294','False Negatives: 321','False Positives: 8,338','True Negatives: 4,531','True Positives: 3,759','False Negatives: 856','False Positives: 2,925','True Negatives: 9,944']
source = [0,0,1,1,2,2,3,3]
target = [4,5,5,4,6,7,7,6]
value = [3598,696,160,161,2726,5612,4332,199]

color_link = ['#b8e186','#d01c8b','#f1b6da','#4dac26','#f1b6da','#4dac26','#b8e186','#d01c8b']

link = dict(source = source, target = target, value = value, color = color_link)
node = dict(label = label, pad=50, thickness=5,color='black',x=[0.1,0.1,0.1,0.1,0.9,0.9,0.9,0.9],y=[0.1,0.3,0.5,0.9,0.1,0.3,0.415,0.815])
data = go.Sankey(link=link,node=node)

fig = go.Figure(data)

fig.update_layout(title={'text': '<span style="font-size: 18px;">Molecular Functions before and after CrowdGO</span>','y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'},font=dict(size=18,color='black'))


fig.write_image(sys.argv[1])
#plotly.offline.plot(fig, filename='yo')
