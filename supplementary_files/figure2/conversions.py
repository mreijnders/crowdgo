import sys
from collections import defaultdict

trueDict = defaultdict(set)
for line in open(sys.argv[1]):
	prot,go = line.strip().split('\t')
	if not go == 'GO:0005575' and not go == 'GO:0003674' and not go == 'GO:0008150':
		trueDict[prot].add(go)

threshold = float(sys.argv[3])
inputDict = defaultdict(dict)
for line in open(sys.argv[2]): ## input preds
	prot,go,score = line.strip().split('\t')
	score = float(score)
	trueBool = False
	if go in trueDict[prot]:
		trueBool = True
	if trueBool == False:
		if score >= threshold:
			inputDict[prot][go] = 'FP'
		else:
			inputDict[prot][go] = 'TN'
	else:
		if score >= threshold:
			inputDict[prot][go] = 'TP'
		else:
			inputDict[prot][go] = 'FN'

threshold = float(sys.argv[5])
outputDict = defaultdict(dict)
for line in open(sys.argv[4]):
	prot,go,score = line.strip().split('\t')
	score = float(score)
	trueBool = False
	if go in trueDict[prot]:
		trueBool = True
	if trueBool == False:
		if score >= threshold:
			outputDict[prot][go] = 'FP'
		else:
			outputDict[prot][go] = 'TN'
	else:
		if score >= threshold:
			outputDict[prot][go] = 'TP'
		else:
			outputDict[prot][go] = 'FN'

tpTp = 0
tpFn = 0
fpFp = 0
fpTn = 0
fnFn = 0
fnTp = 0
tnTn = 0
tnFp = 0
dict = defaultdict(int)
dict2 = defaultdict(int)
for prot,preds in outputDict.items():
	for pred in preds:
		if prot in inputDict and pred in inputDict[prot]:
			outputPred = outputDict[prot][pred]
			inputPred = inputDict[prot][pred]
			if inputPred == 'TP' and outputPred == 'TP':
				tpTp += 1
			elif inputPred == 'TP' and outputPred == 'FN':
				tpFn += 1
			elif inputPred == 'FP' and outputPred == 'FP':
				fpFp += 1
			elif inputPred == 'FP' and outputPred == 'TN':
				fpTn += 1
			elif inputPred == 'FN' and outputPred == 'FN':
				fnFn += 1
			elif inputPred == 'FN' and outputPred == 'TP':
				fnTp += 1
			elif inputPred == 'TN' and outputPred == 'TN':
				tnTn += 1
			elif inputPred == 'TN' and outputPred == 'FP':
				tnFp += 1
				dict[pred] += 1

print('tptp: '+str(tpTp))
print('tpfn: '+str(tpFn))
print('fpfp: '+str(fpFp))
print('fptn: '+str(fpTn))
print('fnfn: '+str(fnFn))
print('fntp: '+str(fnTp))
print('tntn: '+str(tnTn))
print('tnfp: '+str(tnFp))
print('tnfp-fpnt: '+str(tnFp-fpTn))
print('tpfn-fntp: '+str(tpFn-fnTp))

#sortedDict = {k: v for k, v in sorted(dict.items(), key=lambda item: item[1])}
#for val in sortedDict:
#	print(val,dict[val])
