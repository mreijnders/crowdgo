import sys
import plotly.graph_objects as go
import plotly

label = ['True Positives: 40,606','False Negatives: 11,867','False Positives: 126,663','True Negatives: 158,807','True Positives: 38,105','False Negatives: 14,323','False Positives: 18,607','True Negatives: 266,863']
source = [0,0,1,1,2,2,3,3]
target = [4,5,5,4,6,7,7,6]
value = [31261,9345,4978,6889,15519,111144,155719,3088]

color_link = ['#b8e186','#d01c8b','#f1b6da','#4dac26','#f1b6da','#4dac26','#b8e186','#d01c8b']

link = dict(source = source, target = target, value = value, color = color_link)
node = dict(label = label, pad=50, thickness=5,color='black',x=[0.1,0.1,0.1,0.1,0.9,0.9,0.9,0.9],y=[0.1,0.3,0.5,0.9,0.1,0.3,0.39,0.79])
data = go.Sankey(link=link,node=node)

fig = go.Figure(data)

fig.update_layout(title={'text': '<span style="font-size: 18px;">Biological Processes before and after CrowdGO</span>','y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'},font=dict(size=18,color='black'))

fig.write_image(sys.argv[1])
#plotly.offline.plot(fig, filename='yo')
