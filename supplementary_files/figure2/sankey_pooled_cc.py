import sys
import plotly.graph_objects as go
import plotly

label = ['True Positives: 17,330','False Negatives: 3,062','False Positives: 22,453','True Negatives: 1,778','True Positives: 24,665','False Negatives: 3,461','False Positives: 10,794','True Negatives: 29,437']
source = [0,0,1,1,2,2,3,3]
target = [4,5,5,4,6,7,7,6]
value = [15583,1747,1714,1348,9082,13371,16066,1712]

color_link = ['#b8e186','#d01c8b','#f1b6da','#4dac26','#f1b6da','#4dac26','#b8e186','#d01c8b']

link = dict(source = source, target = target, value = value, color = color_link)
node = dict(label = label, pad=50, thickness=5,color='black',x=[0.1,0.1,0.1,0.1,0.9,0.9,0.9,0.9],y=[0.1,0.3,0.5,0.9,0.1,0.3,0.435,0.835])
data = go.Sankey(link=link,node=node)

fig = go.Figure(data)

fig.update_layout(title={'text': '<span style="font-size: 18px;">Cellular Components before and after CrowdGO','y':0.9,'x':0.5,'xanchor': 'center','yanchor': 'top'},font=dict(size=18,color='black'))


fig.write_image(sys.argv[1])
#plotly.offline.plot(fig, filename='yo')
