import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import seaborn as sns
import pandas as pd

df = pd.read_table(sys.argv[1])


sns.set(style="whitegrid",font_scale=1.2)
ax = sns.barplot(x="Method", y="Counts", hue='Type', data=df)
ax.set_title(sys.argv[3])
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles=handles[0:], labels=labels[0:])
ax.set(xlabel='', ylabel='True positives (dark) and false positives (light)')
ax.grid(False)
ax.plot()
fig = ax.get_figure()
fig.tight_layout()
fig.savefig(sys.argv[2],dpi=1200)
