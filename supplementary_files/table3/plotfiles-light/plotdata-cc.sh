#!/bin/bash

./newEval.py /fatty/wei2go/data_old/goCounts.tab /fatty/wei2go/data_old/nameSpaces.tab /fatty/wei2go/data_old/goChildren.tab testset.cc.type1.tab test.cc.uppropagated.type1.tab CrowdGO ../goParents.tab > plotfiles/crowdgo.cc.tab
cat plotfiles/header.tab > plotfiles/all.cco.tab
cat plotfiles/*cc.tab >> plotfiles/all.cco.tab
python lineplot.py plotfiles/all.cco.tab 'Cellular Component' cc.pdf
