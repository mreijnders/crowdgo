#!/bin/bash

./newEval.py /fatty/wei2go/data/goCounts.tab /fatty/wei2go/data/nameSpaces.tab /fatty/wei2go/data/goChildren.tab testset.bp.uppropagated.type1.tab test.bp.uppropagated.type1.tab CrowdGO ../goParents.tab > plotfiles/crowdgo.bp.tab
cat plotfiles/header.tab > plotfiles/all.bpo.tab
cat plotfiles/*bp.tab >> plotfiles/all.bpo.tab
python lineplot.py plotfiles/all.bpo.tab 'Biological Processes' bp.pdf
