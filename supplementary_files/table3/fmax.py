import sys

fmax = 0
threshold = 0
for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	precision = ssline[0]	
	recall = ssline[1]
	tool = ssline[2]
	t = ssline[3]
	precision = float(precision)
	recall = float(recall)
	if not precision == 0 and not recall == 0:
		fscore = 2*((precision*recall)/(precision+recall))
		if fscore > fmax:
			fmax = fscore
			threshold = t
print(fmax,threshold)
