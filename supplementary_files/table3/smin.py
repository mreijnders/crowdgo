#!/usr/bin/python
import sys

smin = 99999
tmax = 0
for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	s = float(ssline[6])
	t = float(ssline[7])
	if s < smin:
		smin = s
		tmax = t
print(smin,tmax)
