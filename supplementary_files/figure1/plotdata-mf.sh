#!/bin/bash

./newEval.py /fatty/wei2go/data/goCounts.tab /fatty/wei2go/data/nameSpaces.tab /fatty/wei2go/data/goChildren.tab testset.mf.uppropagated.tab test.mf.uppropagated.tab CrowdGO ../goParents.tab > plotfiles/crowdgo.mf.tab
cat plotfiles/header.tab > plotfiles/all.mfo.tab
cat plotfiles/*mf.tab >> plotfiles/all.mfo.tab
python lineplot.py plotfiles/all.mfo.tab 'Molecular Function' mf.png
