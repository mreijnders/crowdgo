import matplotlib
matplotlib.use('Agg')
import sys
import seaborn as sns
import pandas as pd
import matplotlib as plt
import numpy as np

df = pd.read_table(sys.argv[1])

sns.set_context('notebook',font_scale=1.2)
palette = sns.color_palette('Paired')

colour_dict = {'CrowdGO':palette[1],'DeepGOPlus':palette[3],'FunFams':palette[5],'InterProScan':palette[7],'Wei2GO':palette[9]}

barplot = sns.barplot(x="Ontology", y="Fmax", hue="Tool", palette=colour_dict, data=df)

barplot.set(ylim=(0,1))
barplot.set(xlabel='',title=sys.argv[2])
handles, labels = barplot.get_legend_handles_labels()
barplot.legend(handles=handles[0:], labels=labels[0:])
barplot.plot()
fig = barplot.get_figure()
fig.tight_layout()
fig.savefig(sys.argv[3],dpi=1200)
