import matplotlib
matplotlib.use('Agg')
import sys
import pandas as pd
import seaborn as sns
import matplotlib as plt

df = pd.read_table(sys.argv[1])
sns.set_context('notebook',font_scale=1.2)
palette = sns.color_palette('Paired')
colour_dict = {'CrowdGO':palette[1],'DeepGOPlus':palette[3],'FunFams':palette[5],'InterProScan':palette[7],'Wei2GO':palette[9]}
lineplot = sns.lineplot(x="Recall",y="Precision",hue='Tool',palette=colour_dict,data=df)
lineplot.set(xlim=(0,1))
lineplot.set(ylim=(0,1))
lineplot.set(xlabel='Recall',ylabel='Precision',title=sys.argv[2])
lineplot.legend(loc=1)
lineplot.plot()
handles, labels = lineplot.get_legend_handles_labels()
lineplot.legend(handles=handles[0:], labels=labels[0:])
fig = lineplot.get_figure()
fig.savefig(sys.argv[3],dpi=1200)
