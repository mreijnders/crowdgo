#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')
import sys
from collections import defaultdict
import seaborn as sns
import matplotlib.pyplot as plt
from math import log10
from math import sqrt

def getGOCounts(goCountPath):
	goCountDict = defaultdict(int)
	for line in open(goCountPath):
		go,count = line.strip().split('\t')
		goCountDict[go] = int(count)
	return(goCountDict)

def getNamespaceDict(namespaceDictPath):
	namespaceDict = defaultdict(str)
	for line in open(namespaceDictPath):
		go,namespace = line.strip().split('\t')
		namespaceDict[go] = namespace
	return(namespaceDict)

def getChildrenDict(childrenDictPath):
	childrenDict = defaultdict(set)
	for line in open(childrenDictPath):
		parent,child = line.strip().split('\t')
		childrenDict[parent].add(child)
	return(childrenDict)

def getNamespaceGOCount(goCountDic,nameSpaceDic):
        namespaceGoCountDic = defaultdict(int)
        bpCount = 0
        mfCount = 0
        ccCount = 0
        for go,count in goCountDic.items():
                namespace = nameSpaceDic[go]
                if namespace == 'biological_process':
                        bpCount += int(count)
                elif namespace == 'molecular_function':
                        mfCount += int(count)
                elif namespace == 'cellular_component':
                        ccCount += int(count)
        namespaceGoCountDic['cellular_component'] = ccCount
        namespaceGoCountDic['molecular_function'] = mfCount
        namespaceGoCountDic['biological_process'] = bpCount
        return(namespaceGoCountDic)

def calcIC(go,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic):
        nameSpace = nameSpaceDic[go]
        if not nameSpace == 'biological_process' and not nameSpace == 'molecular_function' and not nameSpace == 'cellular_component':
                nameSpace = 'biological_process'
        nameSpaceGOCount = namespaceGoCountDic[nameSpace]
        goCount = goCountDic[go]
        nameSpaceGOCount = namespaceGoCountDic[nameSpace]
        ic = 0
        children = childDic[go]
        for child in children:
                if child in goCountDic:
                        goCount += goCountDic[child]
        if goCount == 0:
                goCount = 1
        ic = -log10(float(goCount)/nameSpaceGOCount)
        return(ic)


def evaluate_annotations(real_annots, pred_annots,threshold,childDict,goCountDict,namespaceDict,rootCountDict):
    total = 0
    p = 0.0
    r = 0.0
    p_total= 0
    fps = []
    fns = []
    ttpnSet = set()
    for key in real_annots:
        real_annots_set = set(real_annots[key])
        pred_annots_set = set()
        for vals in pred_annots[key]:
                for val in vals:
                        go,score = vals
                        if score >= threshold:
                                pred_annots_set.add(go)
        if len(real_annots_set) == 0:
            continue
        tp = set(real_annots_set).intersection(set(pred_annots_set))
        for part in tp:
            ttpnSet.add(key+'='+part)
        fp = pred_annots_set - tp
        fn = real_annots_set - tp
        fps.append(fp)
        fns.append(fn)
        tpn = len(tp)
        fpn = len(fp)
        fnn = len(fn)
        total += 1
        recall = tpn / (1.0 * (tpn + fnn))
        r += recall
        if not tpn == 0:
            p_total += 1
            precision = tpn / (1.0 * (tpn + fpn))
            p += precision
    r /= total
    if p_total > 0:
        p /= p_total
    f = 0.0
    if p + r > 0:
        f = 2 * p * r / (p + r)
    ru = 0
    mi = 0
    for gos in fns:
        for go in gos:
            ru += calcIC(go,childDict,goCountDict,namespaceDict,rootCountDict)
    for gos in fps:
        for go in gos:
            mi += calcIC(go,childDict,goCountDict,namespaceDict,rootCountDict)
    ru /= total
    mi /= total
    s = sqrt(ru*ru + mi*mi)
    return(f, p, r, ttpnSet, ru, mi, s)

goCountDict = getGOCounts(sys.argv[1])
namespaceDict = getNamespaceDict(sys.argv[2])
rootCountDict = getNamespaceGOCount(goCountDict,namespaceDict)
childrenDict = getChildrenDict(sys.argv[3])

trueDict = defaultdict(list)
predDict = defaultdict(list)
root_terms = ['GO:0008150','GO:0005575','GO:0003674','GO:0000004','GO:0007582','GO:0044699','GO:0008372','GO:0005554']
for line in open(sys.argv[4]):
	prot,go = line.strip().split('\t')
	if not go in root_terms:
		trueDict[prot].append(go)
for line in open(sys.argv[5]):
	prot,go,score = line.strip().split('\t')
	if not go in root_terms:
		predDict[prot].append([go,float(score)])

fmax = 0
tmax = 0
rList = []
pList = []
prDict = {}
for i in range(0,1000):
	i = float(i)/1000
	f,p,r,ttpnSet,ru,mi,s = evaluate_annotations(trueDict,predDict,i,childrenDict,goCountDict,namespaceDict,rootCountDict)
	prDict[p] = ttpnSet
	p = float(p)
	smin = 99999
	if f > fmax:
		fmax = f
		tmax = i
	if s < smin:
		smin = s
		smint = i
	print(str(p)+'\t'+str(r)+'\t'+sys.argv[6]+'\t'+str(tmax)+'\t'+str(mi)+'\t'+str(ru)+'\t'+str(s)+'\t'+str(smint))

#tot = 0
#for key,values in trueDict.items():
#	tot += len(values)

#tttpnSet = set()
#for p,ttpnSet in reversed(sorted(prDict.items())):
#	for part in ttpnSet:
#		tttpnSet.add(part)
#	recall = len(tttpnSet)/tot
#	print(str(p)+'\t'+str(recall)+'\t'+sys.argv[3])
