#!/usr/bin/env python3
from collections import defaultdict
import sys

namespace_dict = defaultdict(str)
for line in open(sys.argv[3]):
	go,namespace = line.strip().split('\t')
	namespace_dict[go] = namespace

given_namespace = sys.argv[4]
parents_dict = defaultdict(set)
children_dict = defaultdict(set)
for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	if len(ssline) > 1:
		go,parents = ssline
		go = go.strip()
		for parent in parents.split(','):
			children_dict[parent].add(go)
			parents_dict[go].add(parent)

annot_dict = defaultdict(set)
for line in open(sys.argv[2]):
	prot,go = line.strip().split('\t')
	annot_dict[prot].add(go)

tot_parents = 0
tot_go = 0

annot_dict_propagated = defaultdict(set)
annot_dict_leafs = defaultdict(set)
for prot,gos in annot_dict.items():
	for go in gos:
		annot_dict_propagated[prot].add(go)
		namespace = namespace_dict[go]
		if namespace == given_namespace:
			bool = False
			children = children_dict[go]
			for child in children:
				if child in gos and not child == go:
					bool = True
					break
			if bool == False:
				annot_dict_leafs[prot].add(go)
			for parent in parents_dict[go]:
				annot_dict_propagated[prot].add(parent)

for prot,gos in annot_dict_propagated.items():
	for go in gos:
		tot_parents += 1


for prot,gos in annot_dict_leafs.items():
	for go in gos:
		tot_go += 1

print(tot_parents/len(annot_dict),tot_go/len(annot_dict),len(annot_dict),len(annot_dict_propagated.keys()),len(annot_dict_leafs))
