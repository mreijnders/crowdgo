configfile: 'config.yaml'

from datetime import date
from datetime import datetime
import shutil

config['tmpFolder'] = config['outputFolder']+'/'+config['tmpFolder']

if not os.path.isdir(config['outputFolder']):
	os.mkdir(config['outputFolder'])

if not os.path.isdir(config['tmpFolder']):
	os.mkdir(config['tmpFolder'])

rule final:
	input: config['outputFolder']+'/CrowdGO_mf/',
		config['outputFolder']+'/CrowdGO_bp/',
		config['outputFolder']+'/CrowdGO_cc/'

rule DeepGOPlus:
	input: fastaFile = config['fastaFile']
	output: config['outputFolder']+'/deepgoplus.tab'
	params: DeepGOPlusFolder = config['DeepGOPlusFolder'],
		tmpFolder = config['tmpFolder'],
		CrowdGOFolder = config['CrowdGOFolder'],
		diamondPath = config['DiamondBinaryPath']
	run: 	
		shell("{params.diamondPath} blastp -d {params.DeepGOPlusFolder}/data/train_data.dmnd -q {input} --outfmt 6 qseqid sseqid bitscore > {params.tmpFolder}/deepgoplus.diamond.tab")
		shell("python {params.DeepGOPlusFolder}/predict.py -if {input.fastaFile} -of {params.tmpFolder}/deepgoplus.out -df {params.tmpFolder}/deepgoplus.diamond.tab")
		shell("{params.CrowdGOFolder}/scripts/parseDeepGO.py {params.tmpFolder}/deepgoplus.out > {output}")

rule FunFams:
	input: fastaFile =  config['fastaFile']
	output: config['outputFolder']+'/funfams.tab'
	params: FunFamsFolder = config['FunFamsFolder'],
		CrowdGOFolder = config['CrowdGOFolder'],
		tmpFolder = config['tmpFolder'],
	run:
		if not os.path.isdir(config['tmpFolder']+'/funfams/'):
			shell("mkdir {params.tmpFolder}/funfams")
		if not os.path.isdir(config['tmpFolder']+'/funfams/funfams_data'):
			shell("mkdir {params.tmpFolder}/funfams/funfams_data")
		shell("{params.FunFamsFolder}/apps/cath-genomescan.pl -i {input} -l {params.FunFamsFolder}/data/funfam-hmm3-v4_2_0.lib -o {params.tmpFolder}/funfams/")
		shell("{params.CrowdGOFolder}/scripts/retrieveGOForFunFam.py {params.tmpFolder}/funfams/*crh > {params.tmpFolder}/funfams/funfams.domains.tab")
		shell("{params.CrowdGOFolder}/scripts/scanFunFams.py {params.tmpFolder}/funfams/*crh {params.tmpFolder}/funfams/funfams_data {params.FunFamsFolder}")
		shell("{params.CrowdGOFolder}/scripts/parseFunFams.py {params.tmpFolder}/funfams/funfams.domains.tab {params.tmpFolder}/funfams/funfams_data/ > {output}")

if config['mode'].upper() == 'FULL' or config['mode'].upper() == 'FULL-SWISSPROT':
	rule InterProScan:
		input: fastaFile = config['fastaFile']
		output: config['outputFolder']+'/interproscan.tab'
		params: InterProScanFolder = config['InterProScanFolder'],
			CrowdGOFolder = config['CrowdGOFolder'],
			tmpFolder = config['tmpFolder']
		run:
			shell("{params.InterProScanFolder}/interproscan.sh --goterms -i {input} -o {params.tmpFolder}/interproscan.full.tab -f tsv")
			shell("{params.CrowdGOFolder}/scripts/parseIPRScan.py {params.tmpFolder}/interproscan.full.tab > {output}")

rule DIAMOND:
	input: fastaFile = config['fastaFile']
	output: diamondFile = config['outputFolder']+'/diamond.tab'
	params: diamondBinary = config['DiamondBinaryPath'],
		knowledgebaseDBPath = config['knowledgebaseDBPath']
	shell: "{params.diamondBinary} blastp --query {input} --out {output} --db {params.knowledgebaseDBPath}"

rule HMMScan:
	input: fastaFile = config['fastaFile']
	output: hmmscanFile = config['outputFolder']+'/hmmscan.out'
	params: pfamDBPath = config['PfamDBPath']
	shell: "hmmscan --tblout {output} {params.pfamDBPath} {input}"

rule Wei2GO:
	input: diamondFile = config['outputFolder']+'/diamond.tab',
		hmmscanFile = config['outputFolder']+'/hmmscan.out'
	output: config['outputFolder']+'/wei2go.tab'
	params: Wei2GOFolder = config['Wei2GOFolder']
	shell: "python3 {params.Wei2GOFolder}/wei2go.py {input.diamondFile} {input.hmmscanFile} {output}"

if config['mode'].upper() == 'FULL' or config['mode'].upper() == 'FULL-SWISSPROT':
	rule preprocessFull:
		input: DeepGOPlusFile = config['outputFolder']+'/deepgoplus.tab',
			FunFamsFile = config['outputFolder']+'/funfams.tab',
			InterProScanFile = config['outputFolder']+'/interproscan.tab',
			Wei2GOFile = config['outputFolder']+'/wei2go.tab'
		output: outputMF = config['tmpFolder']+'/crowdgoInput.mf.tab',
			outputCC = config['tmpFolder']+'/crowdgoInput.cc.tab',
			outputBP = config['tmpFolder']+'/crowdgoInput.bp.tab'
		params: CrowdGOFolder = config['CrowdGOFolder']
		run:
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} {input.InterProScanFile} molecular_function > {output.outputMF}")
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} {input.InterProScanFile} cellular_component > {output.outputCC}")
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} {input.InterProScanFile} biological_process > {output.outputBP}")

if config['mode'].upper() == 'LIGHT' or config['mode'].upper() == 'LIGHT-SWISSPROT':
	rule preprocessLight:
		input: DeepGOPlusFile = config['outputFolder']+'/deepgoplus.tab',
			FunFamsFile = config['outputFolder']+'/funfams.tab',
			Wei2GOFile = config['outputFolder']+'/wei2go.tab'
		output: outputMF = config['tmpFolder']+'/crowdgoInput.mf.tab',
			outputCC = config['tmpFolder']+'/crowdgoInput.cc.tab',
			outputBP = config['tmpFolder']+'/crowdgoInput.bp.tab'
		params: CrowdGOFolder = config['CrowdGOFolder']
		run:
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions_light.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} molecular_function > {output.outputMF}"),
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions_light.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} molecular_function > {output.outputCC}"),
			shell("{params.CrowdGOFolder}/scripts/concatenatePredictions_light.py {params.CrowdGOFolder}/data/nameSpaces.tab {input.Wei2GOFile} {input.DeepGOPlusFile} {input.FunFamsFile} molecular_function > {output.outputBP}")

rule CrowdGO:
	input: mf = config['tmpFolder']+'/crowdgoInput.mf.tab',
		bp = config['tmpFolder']+'/crowdgoInput.bp.tab',
		cc = config['tmpFolder']+'/crowdgoInput.cc.tab'
	output: mfFile = directory(config['outputFolder']+'/CrowdGO_mf/'),
		bpFile = directory(config['outputFolder']+'/CrowdGO_bp/'),
		ccFile = directory(config['outputFolder']+'/CrowdGO_cc/'),
	params: modelFilePathMF = config['ModelFolder']+'/modelMF.pkl',
		modelFilePathBP = config['ModelFolder']+'/modelBP.pkl',
		modelFilePathCC = config['ModelFolder']+'/modelCC.pkl',
		CrowdGOFolder = config['CrowdGOFolder']
	run: 
		shell("python3 {params.CrowdGOFolder}/CrowdGO.py -i {input.mf} -o {output.mfFile} -m {params.modelFilePathMF}"),
		shell("python3 {params.CrowdGOFolder}/CrowdGO.py -i {input.bp} -o {output.bpFile} -m {params.modelFilePathBP}"),
		shell("python3 {params.CrowdGOFolder}/CrowdGO.py -i {input.cc} -o {output.ccFile} -m {params.modelFilePathCC}")
