#!/usr/bin/env python3
import sys

for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	prot = ssline[0]
	for part in ssline[1::]:
		go,score = part.split('|')
		print(prot+'\t'+go+'\t'+score)
