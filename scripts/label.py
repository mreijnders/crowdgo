#!/usr/bin/env python3
import sys
from collections import defaultdict
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-t','--testset',help='Testset file',required=True,action='store')
parser.add_argument('-o','--output',help='Output file',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
testFilePath = args.testset
outputFilePath = args.output

trueDict = defaultdict(list)
for line in open(testFilePath):
	protein,go = line.strip().split('\t')
	trueDict[protein].append(go)

outFile = open(outputFilePath,'w')
for line in open(inputFilePath):
	method,prot,go,score = line.strip().split('\t')
	label = 'False'
	print(trueDict[prot])
	if go in trueDict[prot]:
		label = 'True'
	outFile.write(line.strip()+'\t'+label+'\n')
outFile.close()
