#!/usr/bin/env python3
import sys
from collections import defaultdict

for line in open(sys.argv[1]):
	prot,funfamFull,score,boundaries = line.strip().split('\t')
	goTermSet = set()
	for boundary in boundaries.split(','):
		start,stop = boundary.split('-')
		funfam,member = funfamFull.split('/FF/')
		goCountDict = defaultdict(int)
		tot = 0
		for line in open(sys.argv[2]+'/'+funfam+'.'+member+'.GO.anno'):
			if not line.startswith('FUN'):
				tot += 1
				ssline = line.strip().split(' ')
				hitBoundaries = ssline[0].split('/')[1].split('_')
				for part in ssline:
					if part.startswith('GO:'):
						goCountDict[part.strip().strip(';')] += 1
				for hitBoundary in hitBoundaries:
					hitStart,hitStop = hitBoundary.split('-')
					if start >= hitStart and start <= hitStop:
						for part in ssline:
							if part.startswith('GO:'):
								goTermSet.add(part.strip().strip(';'))
					elif stop >= hitStart and stop <= hitStop:
						goTermSet = set()
						for part in ssline:
							if part.startswith('GO:'):
								goTermSet.add(part.strip().strip(';'))
	for go in goTermSet:
		score = goCountDict[go]/tot
		print(prot+'\t'+go+'\t'+str(score))
