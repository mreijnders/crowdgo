#!/usr/bin/env python3
import sys

nameSpaceDict = {}
for line in open(sys.argv[1]):
	go,namespace = line.strip().split('\t')
	if not go.startswith('GO:'):
		go = 'GO:'+go
	nameSpaceDict[go] = namespace

for line in open(sys.argv[2]):
	method,prot,go,score = line.strip().split('\t')
	if go in nameSpaceDict:
		namespace = nameSpaceDict[go]
		if namespace == sys.argv[3]:
			print(line.strip())
