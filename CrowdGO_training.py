##
## CrowdGO_training.py: training a random forest model to use for CrowdGO.py
##
## CrowdGO: a wisdom of the crowd based Gene Ontology annotation tool
## 
## Author: MJMF Reijnders
##

## Script is currently outputting sklearn warnings. This is a fault of CrowdGO but of the sklearn modules which aren't updated yet. Therefore the warnings are suppressed.
import warnings
warnings.filterwarnings("ignore")

import argparse
import copy
import joblib
import numpy as np
import os
import pandas as pd
import time
from collections import defaultdict
#from imblearn.over_sampling import SMOTE
from math import log10
from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import AdaBoostClassifier

##
## Function to read the input arguments: -i <training.tab>, -o <output_dir>.
## Returns: file paths for option -i and option -o, and the True or False value for -b (default = False).
##

def readArguments():
	parser = argparse.ArgumentParser(description='CrowdGO protein function prediction')
	parser.add_argument('-i','--input',help='Input training file. Tab separated. Column 1 = prediction method, column 2 = protein ID, column 3 = GO term prediction ("GO:<number>"), column 4 = confidence interval, 5 = labels (True/False)',required=True,action='store')
	parser.add_argument('-o','--output',help='Output dir to store the random forest model needed for predicting GO terms, and intermediate files',required=True,action='store')
	parser.add_argument('-b','--bootstrap',help='Set True to perform bootstrapping to assess model precision recall',required=False,action='store',default='False')
	args = parser.parse_args()
	inputFilePath = args.input
	outputDirPath = args.output
	bootstrapBool = args.bootstrap
	return(inputFilePath,outputDirPath,bootstrapBool)

##
## Reads the <input.tab> file, returns a dictionary with each GO annotation method as key and a list in a list containing 1 protein, 1 GO, and 1 score for each annotation.
## Returns: A dictionary containing the predictions (values) for each method (key), sorted by method in upper case.
##

def readInput(inputFilePath):
	infile = open(inputFilePath)
	predictionDictionary = defaultdict(dict)
	for line in infile.readlines():
		method,protein,go,score,label = line.strip().split('\t')
		method = method.upper()
		#if not [protein,go,score,label] in predictionDictionary[method]:
		predictionDictionary[method].setdefault(protein,set([]))
		predictionDictionary[method][protein].add((go,score,label))
	infile.close()
	return(predictionDictionary)

##
## Function reading the data/goRelations.tab file. Returns three dictionaries: a dictionary with all the parent GO terms for a GO term, a dictionary with all the child GO terms for a go term, and a dictionary with all the parent and child terms for a GO term (goSlimDic).
## Returns: Three dictionaries of GO term (key) and related GO terms in a list (value). Parents dictionary = child + parent according to the GO DAG, child dictionary = parent + child according to the GO dag, and goSlimDic is the two combined
##

def goSlim(crowdGOPath):
    goRelationsFilePath = crowdGOPath+'/data/goRelations.tab'
    goParentsFilePath = crowdGOPath+'/data/goParents.tab'
    goChildrenFilePath = crowdGOPath+'/data/goChildren.tab'
    parentDic = defaultdict(set)
    childDic = defaultdict(set)
    goSlimDic = defaultdict(set)
    goParentsFile = open(goParentsFilePath)
    for line in goParentsFile.readlines():
        child,parent = line.strip().split('\t')
        parentDic[child].add(parent)
        goSlimDic[child].add(parent)
    goParentsFile.close()
    goChildrenFile = open(goChildrenFilePath)
    for line in goChildrenFile.readlines():
        parent,child = line.strip().split('\t')
        childDic[parent].add(child)
        goSlimDic[parent].add(child)
    return(goSlimDic,parentDic,childDic)

##
## Function reading the data/goCounts.tab file. Returns a dictionary containing the amount of times a GO term is assigned to a protein in the UniProt database (via goCounts.tab), and an integer containing the total amount of GO terms assigned to a protein in the UniProt database.
## Returns: Dictionary of GO counts in the UniProt database, and the total number of GO terms
##

def getGoCounts(crowdGOPath):
	totalGoCounts = 0
	goCountDic = defaultdict(int)
	infile = open(crowdGOPath+'/data/goCounts.tab')
	for line in infile.readlines():
		go,count = line.strip().split('\t')
		if not go.startswith('GO:'):
			go = 'GO:'+go
		goCountDic[go] = int(count)
		totalGoCounts += int(count)
	return(goCountDic,totalGoCounts)

##
## Function to retrieve all the name spaces for a GO term: biological process, molecular function, or cellular component. Returns a dictionary with as key the GO term and value the name space.
## Returns: dictionary of GO (key) namespaces (value) (biological_process, molecular_function, cellular_component) according to Gene Ontology's GO.obo file.
##

def getNameSpaces(crowdGOPath):
	nameSpaceDic = defaultdict(str)
	infile = open(crowdGOPath+'/data/nameSpaces.tab')
	for line in infile.readlines():
		go,nameSpace = line.strip().split('\t')
		if not go.startswith('GO:'):
			go = 'GO:'+go
		nameSpaceDic[go] = nameSpace
	return(nameSpaceDic)

def namespaceGOCount(goCountDic,nameSpaceDic):
	namespaceGoCountDic = defaultdict(int)
	bpCount = 0
	mfCount = 0
	ccCount = 0
	for go,count in goCountDic.items():
		namespace = nameSpaceDic[go]
		if namespace == 'biological_process':
			bpCount += int(count)
		elif namespace == 'molecular_function':
			mfCount += int(count)
		elif namespace == 'cellular_component':
			ccCount += int(count)
	namespaceGoCountDic['cellular_component'] = ccCount
	namespaceGoCountDic['molecular_function'] = mfCount
	namespaceGoCountDic['biological_process'] = bpCount
	return(namespaceGoCountDic)

def calcIC(go,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic):
	nameSpace = nameSpaceDic[go]
	nameSpaceGOCount = namespaceGoCountDic[nameSpace]
	goCount = goCountDic[go]
	nameSpaceGOCount = namespaceGoCountDic[nameSpace]
	ic = 0
	children = childDic[go]
	for child in children:
		if child in goCountDic:
			goCount += goCountDic[child]
	if goCount == 0:
		goCount = 1
	ic = -log10(float(goCount)/nameSpaceGOCount)
	return(ic)

def semSim(ic1,ic2):
	icMin = ic1
	if ic1 > ic2:
		icMin = ic2
	semSim = (2*icMin)/(ic1+ic2)
	return(semSim)

def createClusters(predictionDictionary,goParentDic,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic,outDirPath):
	if not os.path.isdir(outDirPath):
		os.makedirs(outDirPath)
	outfileFull = open(outDirPath+'/trainingFeatures.csv','w')
	outfileAbstract = open(outDirPath+'/trainingFeatures_abstract.csv','w')
	outfileLabels = open(outDirPath+'/trainingLabels.csv','w')
	methods = list(predictionDictionary.keys())
	methods.sort()
	for i in range(0,len(methods)):
		method1 = methods[i]
		for protein,predictions in predictionDictionary[method1].items(): ## Loop over the predictions for each method
			for prediction in predictions:
				go1,score1,label1 = prediction ## Extract the values in the prediction
				ic1 = calcIC(go1,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic)
				groupList = [None]*len(methods)
				groupList[i] = [method1,go1,ic1,score1]
				for k in range(0,len(methods)):
					if not k == i:
						method2 = methods[k]
						maxSemanticSimilarity = 0
						maxSemanticSimilarityGO = 'GO:'
						maxSemanticSimilarityScore = 0
						maxSemanticSimilarityIC = 0
						if protein in predictionDictionary[method2]:
							for prediction in predictionDictionary[method2][protein]: ## Loop over the predictions for the 2nd method
								go2,score2,label2 = prediction ## Extract the values in the prediction
								if go2 in goParentDic[go1]:
									ic2 = calcIC(go2,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic)
									semanticSimilarity = semSim(ic1,ic2)
									if semanticSimilarity >= 0.5:
										if semanticSimilarity > maxSemanticSimilarity:
											maxSemanticSimilarity = semanticSimilarity
											maxSemanticSimilarityGO = go2
											maxSemanticSimilarityScore = score2
											maxSemanticSimilarityIC = ic2
						groupList[k] = [method2,maxSemanticSimilarityGO,maxSemanticSimilarityIC,maxSemanticSimilarityScore]
				semSimList = []
				toolCount = 0
				for k in range(0,len(groupList)):
					icK = groupList[k][2]
					for j in range(0,len(groupList)):
						if j > k:
							if not groupList[j][1] == 'GO:':
								icJ = groupList[j][2]
								semanticSimilarityKJ = semSim(icK,icJ)
								semSimList.append(semanticSimilarityKJ)
								toolCount += 1
							else:
								semSimList.append(0)
				outString = protein+','+go1+','+str(ic1)+','+str(toolCount)
				outStringAbstract = str(ic1)+','+str(toolCount)
				for k in range(0,len(groupList)):
					method,go,ic,score = groupList[k]
	#				if ic == 0 and score == 0:
	#					ic = ''
	#					score = ''
					outString += ','+method+','+go+','+str(ic)+','+str(score)
					outStringAbstract += ','+str(ic)+','+str(score)
				for semanticSimilarity in semSimList:
	#				if semanticSimilarity == 0:
	#					semanticSimilarity = ''
					outString += ','+str(semanticSimilarity)
					outStringAbstract += ','+str(semanticSimilarity)
				if label1 == 'True':
					outfileLabels.write('1\n')
				else:
					outfileLabels.write('0\n')
				outfileFull.write(outString+'\n')
				outfileAbstract.write(outStringAbstract+'\n')
	outfileFull.close()
	outfileAbstract.close()
	outfileLabels.close()

##
## Function to execute the machine learning algorithm (xgboost). If the option '-b True' is given, will perform 100x bootstraps to get a rough overview of the models quality, writing the output required for a quick precision recall curve.
## Returns nothing, instead writes 'features.pkl' which is the feature selection required to transform the data before fitting and predicting on the model, and 'model.pkl' which is the trained model.
##

def model(outputDirPath,bootstrapBool):
	print('Training model')
	print('')
	modelTrainingArray = pd.read_csv(outputDirPath+'/trainingFeatures_abstract.csv',header=None)
	modelLabelArray = pd.read_csv(outputDirPath+'/trainingLabels.csv',header=None)
	features_train_res = modelTrainingArray
	labels_train_res = modelLabelArray
	estimators = [('gr',GradientBoostingClassifier(random_state=42)),('rf',RandomForestClassifier(random_state=42)),('ab',AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=2),algorithm='SAMME',n_estimators=500,learning_rate=1,random_state=42)),('lr',LogisticRegression(random_state=42))]
	abModel = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=5),algorithm='SAMME',n_estimators=100000, learning_rate=1.0, random_state=0)
	abModel.fit(features_train_res,labels_train_res)
	print('abmodel done')
	cccvClassifier = CalibratedClassifierCV(abModel,cv='prefit',method='sigmoid')
	modelFit = cccvClassifier.fit(features_train_res,labels_train_res)
	joblib.dump(modelFit,outputDirPath+'/model.pkl')

##
## Below is the workflow of the script.
##

start_time = time.time()
crowdGOPath = os.path.dirname(os.path.abspath(__file__))
inputFilePath,outputDirPath,bootstrapBool = readArguments()
print('Read input files in: '+str(time.time()-start_time)+' seconds',flush=True)
predictionDictionary = readInput(inputFilePath)
print('Created prediction dictionary in: '+str(time.time()-start_time)+' seconds',flush=True)
goSlimDic,parentDic,childDic = goSlim(crowdGOPath)
print('Created relationship dictionary in: '+str(time.time()-start_time)+' seconds',flush=True)
goCountDic,totGoCounts = getGoCounts(crowdGOPath)
print('Created goCounts in: '+str(time.time()-start_time)+' seconds',flush=True)
nameSpaceDic = getNameSpaces(crowdGOPath)
namespaceGoCountDic = namespaceGOCount(goCountDic,nameSpaceDic)
print('Read namespaces in: '+str(time.time()-start_time)+' seconds',flush=True)
clusterDic = createClusters(predictionDictionary,parentDic,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic,outputDirPath)
print('Created overlapping predictions in: '+str(time.time()-start_time)+' seconds',flush=True)
model(outputDirPath,bootstrapBool)
print('Trained model in: '+str(time.time()-start_time)+' seconds',flush=True)
